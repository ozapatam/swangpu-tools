# Omar.Zapata@cern.ch 2019
import sys
from timeit import default_timer as timer

import tensorflow as tf
mnist = tf.keras.datasets.mnist

(x_train, y_train),(x_test, y_test) = mnist.load_data()
x_train, x_test = x_train / 255.0, x_test / 255.0

batch_size = 512
epochs     = 5

start_cpu_train = timer()

with tf.device("/cpu:0"):
  model = tf.keras.models.Sequential([
    tf.keras.layers.Flatten(input_shape=(28, 28)),
    tf.keras.layers.Dense(512, activation=tf.nn.relu),
    tf.keras.layers.Dropout(0.2),
    tf.keras.layers.Dense(10, activation=tf.nn.softmax)
  ])
  model.compile(optimizer='adam',
              loss='sparse_categorical_crossentropy',
              metrics=['accuracy'])

  #batch size increased, by default 32 but it is too small
  model.fit(x_train, y_train, batch_size=batch_size, epochs=epochs) 
end_cpu_train = timer()

start_gpu_train = timer()

with tf.device("/gpu:0"):
  model = tf.keras.models.Sequential([
    tf.keras.layers.Flatten(input_shape=(28, 28)),
    tf.keras.layers.Dense(512, activation=tf.nn.relu),
    tf.keras.layers.Dropout(0.2),
    tf.keras.layers.Dense(10, activation=tf.nn.softmax)
  ])
  model.compile(optimizer='adam',
              loss='sparse_categorical_crossentropy',
              metrics=['accuracy'])

  #batch size increased, by default 32 but it is too small
  model.fit(x_train, y_train, batch_size=batch_size, epochs=epochs) 

end_gpu_train = timer()

print('Times:')
print('batch_size = {}'.format(batch_size))
print('epochs = {}'.format(epochs))
print('CPU Train time (sec):{}'.format(end_cpu_train - start_cpu_train))
print('GPU Train time (sec):{}'.format(end_gpu_train - start_gpu_train))
print('GPU is x{} times faster that CPU'.format((end_cpu_train - start_cpu_train)/(end_gpu_train - start_gpu_train)))
