for i in $(seq 1 1 10);
do
     ssh root@kuboxed-node-$i.cern.ch mount -t nfs kuboxed-gpu-master.cern.ch:/var/kubeVolumes/ /var/kubeVolumes/
     ssh root@kuboxed-node-$i.cern.ch mount -t nfs kuboxed-gpu-master.cern.ch:/mnt/cbox_shares_db/cbox_data /mnt/cbox_shares_db/cbox_data
done
