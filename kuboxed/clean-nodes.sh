read "Are you really sure"
for i in $(seq 1 1 10);
do
     yum remove kube* docker* container*
     ssh root@kuboxed-node-$i.cern.ch kubeadm reset -f
     ssh root@kuboxed-node-$i.cern.ch rm -rf $HOME/.kube/
done
