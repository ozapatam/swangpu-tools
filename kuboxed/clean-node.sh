yum remove kube* container* docker*
rm -rf $HOME/.kube/
rm -rf /var/lib/containerd/
rm -rf /var/lib/docker*
rm -rf /var/lib/etcd/
rm -rf /etc/docker/
rm -rf /etc/kubernetes/ 

rm -rf /etc/ceph \
       /etc/cni \
       /etc/kubernetes \
       /opt/cni \
       /opt/rke \
       /run/secrets/kubernetes.io \
       /run/calico \
       /run/flannel \
       /var/lib/calico \
       /var/lib/etcd \
       /var/lib/cni \
       /var/lib/kubelet \
       /var/lib/rancher/rke/log \
       /var/log/containers \
       /var/log/pods \
       /var/run/calico
