kubectl label nodes kuboxed-node-1.cern.ch nodeApp=ldap
kubectl label nodes kuboxed-node-2.cern.ch nodeApp=eos-mgm
kubectl label nodes kuboxed-node-3.cern.ch nodeApp=eos-fst1
kubectl label nodes kuboxed-node-4.cern.ch nodeApp=eos-fst2
kubectl label nodes kuboxed-node-5.cern.ch nodeApp=cernbox
kubectl label nodes kuboxed-node-6.cern.ch nodeApp=cernbox
kubectl label nodes kuboxed-node-7.cern.ch nodeApp=cernbox
kubectl label nodes kuboxed-node-8.cern.ch nodeApp=cernboxgateway
kubectl label nodes kuboxed-node-9.cern.ch nodeApp=swan
kubectl label nodes kuboxed-node-10.cern.ch nodeApp=eos-mq
#kubectl label nodes kuboxed-gpu-master.cern.ch nodeApp=swan
kubectl label nodes tuxito-sft nodeApp=swan-users
